# frozen_string_literal: true

require_relative 'jekyll/log_adapters/tap'

Jekyll.logger = Jekyll::LogAdapters::Tap::Logger.new

module JekyllCommandDecorator
  def process_with_graceful_fail(cmd, options, *klass)
    super
  ensure
    Jekyll.logger.instance_variable_get(:@writer).close
  end
end

Jekyll::Command.singleton_class.prepend JekyllCommandDecorator
