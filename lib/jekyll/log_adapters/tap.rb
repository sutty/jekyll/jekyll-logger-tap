# frozen_string_literal: true

module Jekyll
  module LogAdapters
    # Provides a replacement for Stevenson to use the Test Anything
    # Protocol
    #
    # http://testanything.org/
    module Tap
      class Logger < Jekyll::Stevenson
        # TAP doesn't have that many levels, so we differentiate errors
        # from warnings by color and prefix.
        #
        # @see Logger::Severity
        SEVERITY_FORMAT = {
          'DEBUG' => '#',
          'INFO'  => 'ok -',
          'WARN'  => 'not ok - Warning:',
          'ERROR' => 'not ok - Error:',
          'FATAL' => 'Bail out!',
          'ANY'   => ''
        }

        SEVERITY_COLOR = {
          'DEBUG' => [],
          'INFO'  => %i[green],
          'WARN'  => %i[yellow],
          'ERROR' => %i[red],
          'FATAL' => %i[red bold],
          'ANY'   => []
        }

        COUNT_TESTS = (1..3)

        attr_reader :test_counter

        # Replace the formatter with a proc
        #
        # Multi-line messages are assumed to be debug level, like the
        # profiler table.
        def initialize
          super

          logdevice(1).puts('TAP version 14')

          # Initialize counter
          @test_counter = 0
          # At this point, severity is an Integer
          @formatter = proc do |severity, _, _, msg|
            msg = msg.to_s.strip_ansi.strip

            next if msg.empty?

            if msg.lines.size > 1
              msg.lines.map do |line|
                "#{tap_format(0)} #{line}"
              end.join
            else
              "#{tap_format(severity)} #{msg}"
            end
          end
        end

        def close
          logdevice(0).puts("1..#{@test_counter}")
        end

        private

        # Don't convert severity level to string
        #
        # @see Logger#format_severity
        # @return [Integer]
        def format_severity(severity)
          severity
        end

        # Test if logdevice is a TTY
        #
        # @param :severity [Integer]
        def tty?(severity)
          !!logdevice(severity)&.isatty
        end

        # At this point, severity is an Integer.  Skips colorizing if it
        # detects we're piping output.
        #
        # @param :severity [Integer]
        # @return [String]
        def tap_format(severity)
          @test_counter += 1 if COUNT_TESTS.include? severity
          tap_formatted = SEVERITY_FORMAT.values[severity]

          return tap_formatted unless tty?(severity)

          colors = SEVERITY_COLOR.values[severity]

          colors.reduce(tap_formatted) do |aggregated, color|
            aggregated.public_send(color)
          end
        end
      end
    end
  end
end
