# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = 'jekyll-logger-tap'
  spec.version       = '0.2.0'
  spec.authors       = ['f']
  spec.email         = ['f@sutty.nl']

  spec.summary       = 'TAP logger for Jekyll'
  spec.homepage      = 'https://0xacab.org/sutty/jekyll/jekyll-logger-tap'
  spec.license       = 'Nonstandard'

  spec.files         = Dir['lib/**/*',
                           'LICENSE.txt',
                           'README.md']

  # Archivos que son parte de la documentación
  spec.extra_rdoc_files = Dir['README.md', 'CHANGELOG.md', 'LICENSE.txt']
  # Opciones para el generador de documentación
  spec.rdoc_options += [
    '--title', "#{spec.name} - #{spec.summary}",
    '--main', 'README.md',
    '--line-numbers',
    '--inline-source',
    '--quiet'
  ]

  # Otros metadatos de la gema
  spec.metadata = {
    'bug_tracker_uri'   => "#{spec.homepage}/issues",
    'homepage_uri'      => spec.homepage,
    'source_code_uri'   => spec.homepage,
    'changelog_uri'     => "#{spec.homepage}/-/blob/master/CHANGELOG.md",
    'documentation_uri' => "https://rubydoc.info/gems/#{spec.name}"
  }

  spec.add_runtime_dependency 'jekyll', '~> 4.2'
end
