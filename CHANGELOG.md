# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2023-10-02

### Changed

- Color is removed when the output is piped
- Add TAP version header
- Print test plan at the end

## [0.1.0] - 2023-10-02

### Added

- It works!
