# jekyll-logger-tap

A [Test Anything Protocol](http://testanything.org/) formatter for
Jekyll logs.

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
group :jekyll_plugins do
  gem 'jekyll-logger-tap', require: 'jekyll-logger-tap'
end
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-logger-tap

## Usage

You don't need to do anything else.  Since the `:jekyll_plugins` group
is loaded early by Jekyll, all logging should be following the TAP
format.

## Contributing

Bug reports and pull requests are welcome on 0xacab.org at
<https://0xacab.org/sutty/jekyll/jekyll-logger-tap>. This project is
intended to be a safe, welcoming space for collaboration, and
contributors are expected to adhere to the [Sutty's code of
conduct](http://sutty.nl/en/code-of-conduct).

## License

The theme is available as open source under the terms of the
[Anti-fascist MIT License](LICENSE.txt).
